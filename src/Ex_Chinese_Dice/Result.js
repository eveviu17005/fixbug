import React, { Component } from "react";
import { connect } from "react-redux";
import { playGame, result } from "../redux/action/diceAction";

class Result extends Component {
  render() {
    return (
      <div className="text-center pt-3 display-4">
        <button
          onClick={this.props.handleResult}
          className="btn btn-secondary btn-lg"
        >
          <span>Play Game</span>
        </button>
        <h2>{this.props.resultPlayGame}</h2>
        <div className="pt-4">
          <h2 className="pt-3">Bạn chọn: {this.props.chooseDice}</h2>
          <h3>Số lần bạn thắng : {this.props.totalWin} </h3>
          <h3>Số lần bạn thua :{this.props.totalLose} </h3>
          <h3>Số lần play game : {this.props.totalPlayGame}</h3>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => ({
  // arrayDice: state.diceReducer.arrayDice,
  resultPlayGame: state.diceReducer.resultPlayGame,
  totalWin: state.diceReducer.totalWin,
  totalLose: state.diceReducer.totalLose,
  totalPlayGame: state.diceReducer.totalPlayGame,
  chooseDice: state.diceReducer.chooseDice,
});

let mapDispatcchToProps = (dispatch) => ({
  handleResult: () => {
    dispatch(result());
  },
});

export default connect(mapStateToProps, mapDispatcchToProps)(Result);
