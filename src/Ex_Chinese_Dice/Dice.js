import React, { Component } from "react";
import { connect } from "react-redux";
import { chooseDice } from "../redux/action/diceAction";
import { TAI, XIU } from "../redux/constant/diceConstant";

let stylebtn = {
  with: 100,
};
class Dice extends Component {
  renderDice = () => {
    return this.props.arrayDice.map((item, index) => {
      return (
        <img
          src={item.img}
          alt=""
          style={{ width: 100, margin: 15 }}
          className="rounded"
          key={index}
        />
      );
    });
  };

  render() {
    return (
      <div className="container pt-3">
        <h1 style={{ fontSize: 70 }}>Game Tài Xỉu</h1>
        <div className="d-flex justify-content-between pt-3">
          <button
            onClick={() => {
              this.props.handleChooseDice(TAI);
            }}
            color="danger"
          >
            Tai
          </button>
          <div>{this.renderDice()}</div>
          <button
            onClick={() => {
              this.props.handleChooseDice(XIU);
            }}
            color="dark"
          >
            Xiu
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  arrayDice: state.diceReducer.arrayDice,
});

const mapDispatcchToProps = (dispatch) => ({
  handleChooseDice: (dice) => {
    dispatch(chooseDice(dice));
  },
});

export default connect(mapStateToProps, mapDispatcchToProps)(Dice);
