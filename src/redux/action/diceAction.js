import { CHOOSE_DICE, PLAY_GAME, RESULT } from "../constant/diceConstant";

export const result = () => ({
  type: RESULT,
});

export const chooseDice = (dice) => ({
  type: CHOOSE_DICE,
  payload: dice,
});

export const playGame = (dice) => ({
  type: PLAY_GAME,
  payload: dice,
});
