import { combineReducers } from "redux";
import { diceReducer } from "./diceReducer";

export const rootReducer_Chinese_Dice = combineReducers({ diceReducer });
