import { CHOOSE_DICE, RESULT, TAI, XIU } from "../constant/diceConstant";

let initialState = {
  arrayDice: [
    {
      img: "./imgXucXac/1.png",
      value: 1,
    },
    {
      img: "./imgXucXac/2.png",
      value: 2,
    },
    {
      img: "./imgXucXac/3.png",
      value: 3,
    },
  ],
  totalWin: 0,
  totalLose: 0,
  totalPlayGame: 0,
  chooseDice: "",
  resultPlayGame: "",
};

export const diceReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHOOSE_DICE: {
      return { ...state, chooseDice: payload };
    }
    case RESULT: {
      let point = 0;
      // tạo array mới
      let newArrayDice = state.arrayDice.map(() => {
        // Math.floor chỉ trả từ 0 đến 0.9 nên phải cộng 1
        let random = Math.floor(Math.random() * 6) + 1;
        point += random;
        return {
          img: `./imgXucXac/${random}.png`,
          value: random,
        };
      });

      let totalWin = state.totalWin;
      let totalLose = state.totalLose;
      let totalPlayGame = state.totalPlayGame;
      let resultPlayGame = "";
      if (state.chooseDice == "TAI" && point >= 11) {
        totalWin++;
        resultPlayGame = "You WIN";
      } else if (state.chooseDice == "XIU" && point < 11) {
        totalWin++;
        resultPlayGame = "You WIN";
      } else {
        resultPlayGame = "You Lose";
        totalLose++;
      }
      totalPlayGame = totalWin + totalLose;
      return {
        ...state,
        resultPlayGame,
        totalWin,
        totalLose,
        totalPlayGame: totalPlayGame,
      };
    }
    default:
      return { ...state };
  }
};
